import csv
from typing import List


class Csv:
    header: str = None
    rows: List[str] = None

    def __init__(self, header):
        self.header = header
        self.rows = []

    def add_row(self, row):
        self.rows.append(row)

    def write_to_file(self, path: str):
        """
        Writes out CSV file.

        :param path: Path file name of CSV file to be written to

        """
        with open(path, 'w') as new_file:
            csv_writer = csv.writer(new_file, delimiter='|', quotechar='&', quoting=csv.QUOTE_MINIMAL)

            self.header = self.header.strip('\"')

            csv_writer.writerow([self.header])

            for line in self.rows:
                csv_writer.writerow([line])
