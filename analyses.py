from ephemeral_tweet import EphemeralTweet
from user_login import UserLogin
from typing import Dict, Set
from csv_output import Csv
from datetime import datetime


class Analyses:

    @staticmethod
    def unique_tweet_users(tweets):
        """
        Calculates the number of unique users that used the ephemeral tweet feature.

        :param tweets: Tweets individual ephemeral tweets

        """
        unique_users: Set[int] = set()

        for tweet in tweets:
            unique_users.add(tweet.get_user_id())

        print("Unique Tweet Users: %d\n\n", len(unique_users))

    @staticmethod
    def tweets_per_user(users, tweets, output_path):
        """
        Calculates the number of users that tweet at a certain volume. For example, you would be
        able to answer how many users tweet more than a single time. Outputs in CSV format.

        :param users: Individual user login instances
        :param tweets: Tweets individual ephemeral tweets
        :param output_path: CSV output file name

        """
        num_users_tweets: Dict[int, int] = dict()

        for user in users:
            user_login_id = UserLogin.get_user_id(user)
            num_users_tweets.update({user_login_id: 0})

        for tweet in tweets:
            tweet_count: int = num_users_tweets.get(EphemeralTweet.get_user_id(tweet))
            num_users_tweets.update({EphemeralTweet.get_user_id(tweet): tweet_count + 1})

        num_tweets_to_user_count: Dict[int, int] = dict()

        for tweet_count_per_user in num_users_tweets:
            num_tweets_to_user_count.update({num_users_tweets.get(tweet_count_per_user): 0})

        for user_id in num_users_tweets:
            user_tweet_count: int = num_users_tweets.get(user_id)

            current_user_count: int = num_tweets_to_user_count.get(
                user_tweet_count)

            num_tweets_to_user_count.update({user_tweet_count: current_user_count + 1})

        for key, value in num_tweets_to_user_count.items():
            print(key, value)

        output_csv = Csv('tweet_count,user_count')

        for tweet_volume in num_tweets_to_user_count:
            user_count = num_tweets_to_user_count.get(tweet_volume)

            tweet_volume_count = '{:d},{:d}'.format(tweet_volume, user_count)

            output_csv.add_row(tweet_volume_count)

        output_csv.write_to_file(output_path)

    @staticmethod
    def tweets_per_day(tweets, output_path):
        """
        Calculates the number of ephemeral tweets on a given day. Outputs in CSV format.

        :param tweets: Tweets individual ephemeral tweets
        :param output_path: CSV output file name

        """
        days: Dict[int, int] = dict()

        for tweet_created in tweets:
            date_created: datetime = EphemeralTweet.get_created_date_time(tweet_created)
            day: int = date_created.day
            days.update({day: 0})

        for tweet_created in tweets:
            date_created: datetime = EphemeralTweet.get_created_date_time(tweet_created)
            day = date_created.day

            current_count = days.get(day)

            days.update({day: current_count + 1})

        output_csv = Csv('day,tweet_count')

        for day in days:
            tweet_count = days.get(day)

            tweet_per_day = '{:d},{:d}'.format(day, tweet_count)

            output_csv.add_row(tweet_per_day)

        output_csv.write_to_file(output_path)

    @staticmethod
    def clients_per_user(users, output_path):
        """
        Calculates the number of client applications per unique user. Outputs in CSV format.

        :param users: Users individual user login instances
        :param output_path: CSV output file name

        """
        user_client_apps = dict()

        for user in users:
            if UserLogin.get_user_id(user) not in user_client_apps:
                user_client_apps.update({UserLogin.get_user_id(user): set()})

            client_list_per_user = user_client_apps.get(UserLogin.get_user_id(user))
            client_app = UserLogin.get_client_app(user)
            client_list_per_user.add(client_app)
            user_client_apps.update({UserLogin.get_user_id(user): client_list_per_user})

        output_csv = Csv('user_id,num_client_apps')

        for user_id in user_client_apps:
            client_list = user_client_apps.get(user_id)

            num_clients = len(client_list)
            print(type(user_id))

            num_clients_per_user = '{:d},{:d}'.format(user_id, num_clients)

            output_csv.add_row(num_clients_per_user)

        output_csv.write_to_file(output_path)

    @staticmethod
    def average_clients_per_user_per_country(users, output_path):
        """
        Calculates the average number of client applications per user per country. Outputs in CSV
        format.

        :param users: Users individual user login instances
        :param output_path: CSV output file name

        """
        client_apps = count_client_apps_per_user(users)

        country_origin = origin_country_per_user(users)

        total_client_apps_per_country = dict()

        for user in users:
            total_client_apps_per_country.update({UserLogin.get_user_country(user): [0] * 2})

        for user_id in country_origin:
            curr_countries = country_origin.get(user_id)
            curr_unique_client_app_count = client_apps.get(user_id)

            for country in curr_countries:
                total_running_count = total_client_apps_per_country.get(country)
                total_running_count[0] = total_running_count[0] + curr_unique_client_app_count
                total_running_count[1] = total_running_count[1] + 1

                total_client_apps_per_country.update({country: total_running_count})

        output_csv = Csv('user_country_origin,average_client_apps_per_user_per_user')

        average_client_app = dict()

        for country in total_client_apps_per_country:
            total_running_count = total_client_apps_per_country.get(country)

            average = total_running_count[0] // total_running_count[1]
            average_client_app.update({country: average})

            average_client_apps = '{:s},{:d}'.format(country, average)

            output_csv.add_row(average_client_apps)

        output_csv.write_to_file(output_path)


def origin_country_per_user(users):
    """
    Calculates the total number of client applications per unique user.

    :param users: Users individual user login instances

    :return: A mapping of a unique user to the number of client applications specific to
    that user

    """
    user_to_origin_country = dict()

    for user in users:
        if UserLogin.get_user_id(user) not in user_to_origin_country:
            user_to_origin_country.update({UserLogin.get_user_id(user): set()})

        curr_user_country_list = user_to_origin_country.get(UserLogin.get_user_id(user))
        country = UserLogin.get_user_country(user)
        curr_user_country_list.add(country)
        user_to_origin_country.update({UserLogin.get_user_id(user): curr_user_country_list})

    return user_to_origin_country


def count_client_apps_per_user(users):
    """
    Maps user to its country of origin.

    :param users: Users individual user login instances

    :return: A mapping of unique users to its country of origin

    """
    client_apps = dict()

    for user in users:
        client_apps.update({UserLogin.get_user_id(user): set()})

    for user in users:
        curr_client_app_list = client_apps.get(UserLogin.get_user_id(user))
        client_app = UserLogin.get_client_app(user)
        curr_client_app_list.add(client_app)
        client_apps.update({UserLogin.get_user_id(user): curr_client_app_list})

    num_client_apps_per_user = dict()

    for user_id in client_apps:
        num_client_app = len(client_apps.get(user_id))
        num_client_apps_per_user.update({user_id: num_client_app})

    return num_client_apps_per_user
