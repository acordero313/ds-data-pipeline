from analyses import Analyses
from ephemeral_tweet import EphemeralTweet
from user_login import UserLogin
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file_path_tweets", help="file path for tweets")
    parser.add_argument("file_path_logins", help="file path for logins")

    args = parser.parse_args()

    print(args.file_path_tweets)
    print(args.file_path_logins)


ephemeral_tweet_list: EphemeralTweet = EphemeralTweet.parse_csv(args.file_path_tweets)

Analyses.unique_tweet_users(ephemeral_tweet_list)

user_login_list: UserLogin = UserLogin.parse_csv(args.file_path_logins)

Analyses.tweets_per_user(user_login_list, ephemeral_tweet_list, 'user_tweet_volume.csv')

Analyses.tweets_per_day(ephemeral_tweet_list, 'tweets_per_day.csv')

Analyses.clients_per_user(user_login_list, 'clients_per_user.csv')

Analyses.average_clients_per_user_per_country(user_login_list, 'average_clients_per_user_per_country.csv')


