import datetime
import csv
from typing import List


class UserLogin:
    user_id: int = None
    country: str = None
    client_app: str = None
    login_date: datetime = None

    def get_user_id(self) -> int:
        return self.user_id

    def get_user_country(self) -> str:
        return self.country

    def get_client_app(self) -> str:
        return self.client_app

    def get_user_login_date(self) -> datetime:
        return self.login_date

    @staticmethod
    def parse_csv(path: str):
        """
        Takes in CSV file and parses it into a list of users.

        :param path: Path file name of CSV to be parsed

        :return: List of user login instances
        """
        login_list: List[UserLogin] = []

        with open(path, 'r') as csv_file:
            read_csv = csv.reader(csv_file)

            next(read_csv)

            for row in read_csv:
                user_login: UserLogin = UserLogin.parse_csv_line(row)
                login_list.append(user_login)

        return login_list

    @staticmethod
    def parse_csv_line(split_line: str):
        """
        Parses CSV row into user login object.

        :param split_line: Line CSV row containing user login instances
        :return: User login object containing attributes
        """
        date_format = "%Y-%m-%d"

        user_login: UserLogin = UserLogin()

        user_login.user_id = split_line[1]
        user_login.country = split_line[2]
        user_login.client_app = split_line[3]
        user_login.login_date = datetime.datetime.strptime(
            split_line[4], date_format).date()

        return user_login
