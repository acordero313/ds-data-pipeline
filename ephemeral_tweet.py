import datetime
import csv
from typing import List


class EphemeralTweet:
    created_date_time: datetime = None
    deleted_date_time: datetime = None
    tweet_id: int = None
    user_id: int = None

    def get_created_date_time(self) -> datetime:
        return self.created_date_time

    def get_deleted_date_time(self) -> datetime:
        return self.deleted_date_time

    def get_user_id(self) -> int:
        return self.user_id

    def get_tweet_id(self) -> int:
        return self.tweet_id

    @staticmethod
    def parse_csv(path: str):
        """
        Takes in a CSV file and parses it into a list of Ephemeral Tweets.

        :param path: Path file name of CSV file to be parsed

        :return: List of ephemeral tweet instances
        """
        tweet_list: List[EphemeralTweet] = []

        with open(path, 'r') as csv_file:
            read_csv = csv.reader(csv_file)

            next(read_csv)

            for row in read_csv:
                tweet_data: EphemeralTweet = EphemeralTweet.parse_csv_line(row)
                tweet_list.append(tweet_data)

        return tweet_list

    @staticmethod
    def parse_csv_line(split_line: str):
        """
        Parses CSV row into ephemeral tweet object.

        :param split_line: Line CSV row containing ephemeral tweet

        :return: Ephemeral tweet object containing attributes
        """
        date_format = "%m/%d/%Y %H:%M"

        ephemeral_tweet: EphemeralTweet = EphemeralTweet()

        ephemeral_tweet.created_date_time = datetime.datetime.strptime(split_line[1], date_format)
        ephemeral_tweet.deleted_date_time = datetime.datetime.strptime(split_line[2], date_format)
        ephemeral_tweet.tweet_id = split_line[3]
        ephemeral_tweet.user_id = split_line[4]

        return ephemeral_tweet
